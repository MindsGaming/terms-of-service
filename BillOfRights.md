Bill Of Rights

Minds is officially adopting the Manila Principles On Intermediary Liability, a digital bill of rights, outlined by the Electronic Frontier Foundation and other leading digital rights organizations. The principles have been endorsed by nearly 300 leading press freedom and technology policy organizations and individuals. They act as a guideline for protecting freedom of expression and create an open environment for innovation. It is our hope to create a network effect of companies and organizations adopting the principles to further Internet freedom globally. 

Introduction

All communication over the Internet is facilitated by intermediaries such as Internet access providers, social networks, and search engines. The policies governing the legal liability of intermediaries for the content of these communications have an impact on users’ rights, including freedom of expression, freedom of association and the right to privacy.

With the aim of protecting freedom of expression and creating an enabling environment for innovation, which balances the needs of governments and other stakeholders, civil society groups from around the world have come together to propose this framework of baseline safeguards and best practices. These are based on international human rights instruments and other international legal frameworks.

Uninformed intermediary liability policies, blunt and heavy-handed regulatory measures, and a lack of consistency across these policies has resulted in censorship and other human rights abuses by governments and private parties, limiting individuals’ rights to free expression and creating an environment of uncertainty that also impedes innovation online.

The framework presented here should be considered by policymakers and intermediaries when developing, adopting, and reviewing legislation, policies and practices that govern the liability of intermediaries for third-party content. Our objective is to encourage the development of interoperable and harmonized liability regimes that can promote innovation while respecting users’ rights in line with the Universal Declaration of Human Rights, the International Covenant on Civil and Political Rights and the United Nations Guiding Principles on Business and Human Rights.

The Manila Principles On Intermediary Liability

1. Intermediaries should be shielded from liability for third-party content.

2. Content must NOT be required to be removed without an order by a judicial authority.

3. Requests for restrictions of content must be clear, be unambiguous, and follow due process.

4. Laws and content restriction orders and practices must comply with the tests of necessity and proportionality.

5. Laws and content restriction policies and practices must respect due process.

6. Transparency and accountability must be built into laws and content restriction policies and practices.

Please visit manilaprinciples.org for more information on each principle. 