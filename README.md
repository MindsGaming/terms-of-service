# Minds Policies

- [Terms of Service](https://gitlab.com/minds/terms-of-service/blob/master/TERMS.md)
- [Privacy Policy](https://gitlab.com/minds/terms-of-service/blob/master/PRIVACY.md)
- [Content Policy](http://minds.com/content-policy)
- [Bill Of Rights](https://www.minds.com/p/billofrights)
- [DMCA](https://www.minds.com/p/dmca)